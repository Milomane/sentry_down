﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MonsterHealth : MonoBehaviour
{
    public float health;
    public float maxHealth;

    public GameObject prefabCoin;
    public GameObject prefabBlood;
    public float minCoinLoot;
    public float maxCoinLoot;
    
    void Start()
    {
        health = maxHealth;
    }

    void Update()
    {
        if (health <= 0)
        {
            for (int i = Mathf.RoundToInt(Random.Range(minCoinLoot, maxCoinLoot)); i > 0; i--)
            {
                Instantiate(prefabCoin, transform.position, Quaternion.identity);
            }

            GameObject blood = Instantiate(prefabBlood, transform.position , Quaternion.identity);
            blood.gameObject.transform.Rotate(0.0f, 0.0f, Random.Range(0, 360));

            if (GetComponent<BoomerDeath>())
            {
                GetComponent<BoomerDeath>().Explode();
            }
            
            Destroy(gameObject);
            //Die
        }
    }

    public void takeDamage(int damage)
    {
        health -= damage;
    }
}
