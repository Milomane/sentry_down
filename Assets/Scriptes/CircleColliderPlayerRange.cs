﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleColliderPlayerRange : MonoBehaviour
{
    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            GetComponentInParent<SentryScript>().detectRepair(other);
        }
    }
}
