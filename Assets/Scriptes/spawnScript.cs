﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class spawnScript : MonoBehaviour
{
    public Transform firstTarget;

    public bool waveEnd;

    public List<Wave> waves;

    private float timer;

    public int currentWave;
    public float timeBetweenWave;

    public GameObject waveIncommingAlert;
    
    
    [System.Serializable]
    public class Mobs
    {
        public GameObject mobPrefab;
        public int numberInLevel;
    }

    [System.Serializable]
    public class Wave
    {
        public float minTimeBetweenMonsters;
        public float maxTimeBetweenMonsters;
        public List<Mobs> mobs;
    }
    
    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            timer = Random.Range(waves[currentWave].minTimeBetweenMonsters, waves[currentWave].maxTimeBetweenMonsters);
            Spawn();
        }
    }

    public void Spawn()
    {
        if (!waveEnd)
        {
            if (waves[currentWave].mobs.Count > 0)
            {
                int selected = Mathf.RoundToInt(Random.Range(0, waves[currentWave].mobs.Count-1));

                GameObject enemy = Instantiate(waves[currentWave].mobs[selected].mobPrefab, transform.position, Quaternion.identity);
                waves[currentWave].mobs[selected].numberInLevel--;
                if (waves[currentWave].mobs[selected].numberInLevel <= 0)
                {
                    waves[currentWave].mobs.RemoveAt(selected);
                }
        
                enemy.GetComponent<EnemyMovement>().target = firstTarget;
            }
            else
            {
                if (currentWave != waves.Count - 1)
                {
                    waveEnd = true;
                    StartCoroutine(newWave());
                }
                else
                {
                    waveEnd = true;
                    SceneManager.LoadScene("End");
                    //WIN
                }
            }
        }
    }

    public IEnumerator newWave()
    {
        currentWave++;
        yield return new WaitForSeconds(timeBetweenWave-5);
        
        waveIncommingAlert.SetActive(true);
        
        yield return new WaitForSeconds(1.2f);
        
        waveIncommingAlert.SetActive(false);
        
        yield return new WaitForSeconds(3.8f);
        waveEnd = false;
        
        yield return null;
    }
}
