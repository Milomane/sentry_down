﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathScriptes : MonoBehaviour
{
    public Transform nextTarget;
    
    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<EnemyMovement>().target = nextTarget;
        }
    }
}
