﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarEnemy : MonoBehaviour
{
    private MonsterHealth monsterHealth;
    
    void Start()
    {
        monsterHealth = GetComponentInParent<MonsterHealth>();
    }
    
    void Update()
    {
        float scale = monsterHealth.health / monsterHealth.maxHealth;
        transform.localScale = new Vector3(scale, 1, 1) ;
    }
}