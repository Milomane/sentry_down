﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour
{
    public string sceneToLoad;

    public void goToScene()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
