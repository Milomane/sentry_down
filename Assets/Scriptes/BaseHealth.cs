﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHealth : MonoBehaviour
{
    void Update()
    {
        float scale = FindObjectOfType<Base>().health / FindObjectOfType<Base>().maxHealth;
        transform.localScale = new Vector3(scale, 1, 1);
    }
}
