﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(Rigidbody2D))]

public class PlayerController: MonoBehaviour
{
    public Vector2 input;
    
    //public GameObject starStun;

    private PlayerMovement playerMovement;
    private Rigidbody2D rb;
    private Animator animator;

    public int money;

    public Text coinText;
    
    
    
    //public static PlayerController playerController;

    void Start()
    {
        animator = GetComponent<Animator>();
        playerMovement = GetComponent<PlayerMovement>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        // Input
        input.x = Input.GetAxisRaw("Horizontal"); // Get value between 1 and -1
        input.y = Input.GetAxisRaw("Vertical"); // Get value between 1 and -1

        coinText.text = "Cuivre x 0" + money;
        
        animator.SetFloat("horizontalAxis", input.x);
    }

    /*public IEnumerator Stun(object[] parms)
    {
        float stunTime = (float)parms[0];
        float speedMultiplier = (float)parms[1];
        
        float initSpeed = playerMovement.maxSpeed;
        playerMovement.maxSpeed *= speedMultiplier;
        starStun.SetActive(true);
        yield return new WaitForSeconds(stunTime);
        playerMovement.maxSpeed = initSpeed;
        starStun.SetActive(false);
    }*/

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Coin")
        {
            money++;
            Destroy(other.gameObject);
        }
    }
}