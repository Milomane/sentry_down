﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Base : MonoBehaviour
{
    public float blinckTime;
    private float timer;

    public float maxHealth;
    public float health;

    private bool endStart = false;
    
    void Start()
    {
        health = maxHealth;
    }

    void Update()
    {
        timer -= Time.deltaTime;

        if (!endStart)
        {
            if (health <= 0)
            {
                End();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            StartCoroutine(blinck());
            health -= other.GetComponent<EnemyAttack>().baseDamage;
            
            if (health <= 0)
            {
                StartCoroutine("End");
            }
            
            Destroy(other.gameObject);
        }
    }

    public IEnumerator blinck()
    {
        timer = blinckTime;
        while (timer > 0)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            yield return new WaitForSeconds(0.2f);
            GetComponent<SpriteRenderer>().color = Color.white;
            yield return new WaitForSeconds(0.2f);
        }
    }

    public IEnumerator End()
    {
        endStart = true;
        GetComponent<Animator>().SetBool("End", true);
        
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("GameOver");
    }
}
