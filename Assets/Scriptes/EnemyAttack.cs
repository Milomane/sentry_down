﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public float damage;
    public float attackRate;
    
    private float timer;
    private Animator animator;

    public int baseDamage;
    
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            timer = 0;
        }
    }

    public void Attack(GameObject target)
    {
        if (timer <= 0)
        {
            if (target.GetComponent<SentryScript>().health > 0)
            {
                timer = attackRate;
                target.GetComponent<SentryScript>().takeDamage(damage);
                animator.SetTrigger("Attack");
            }
        }
    }
}
