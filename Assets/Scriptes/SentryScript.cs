﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SentryScript : MonoBehaviour
{
    public float maxHealth;
    public float health;
    public float regenPerSec;
    
    public string name;
    public int level;

    public float shootRate;
    public int damage;
    private float timer;
    private float blinckTimer;
    public GameObject prefabBullet;
    public float bulletSpeed;
    private float animTimer;

    public bool tActive;
    public bool attack;

    private GameObject player;
    private Animator animator;
    
    public Weapon[] weapon;
    public int currentWeapon;
    private float boxTimer;

    public bool playerInRange;

    public GameObject repair;
    public GameObject upgrade;
    public GameObject placing1;
    public GameObject placing2;
    public GameObject placing3;
    
    
    public bool inputSpace;
    public bool inputReturn;
    public bool input1;
    public bool input2;
    public bool input3;

    
    [System.Serializable]
    public class StatPerLevel
    {
        public float shootRate;
        public int damage;
        public int repairPrices;
        public int sentryPrices;
        public float newMaxHealth;
        public float regenPerSec;
    }

    [System.Serializable]
    public class Weapon
    {
        public StatPerLevel[] stat;
        public GameObject prefabBullet;
    }
    
    void Start()
    {
        timer = 0;
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player");
        tActive = false;
    }
    
    void Update()
    {
        inputSpace = Input.GetKeyDown(KeyCode.Space);
        inputReturn = Input.GetKeyDown(KeyCode.Return);
        input1 = Input.GetKeyDown(KeyCode.Alpha1);
        input2 = Input.GetKeyDown(KeyCode.Alpha2);
        input3 = Input.GetKeyDown(KeyCode.Alpha3);
        
        
        if (blinckTimer > 0) blinckTimer -= Time.deltaTime;
        
        
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            timer = 0;
        }

        if (animTimer > 0)
        {
            animTimer -= Time.deltaTime;
            attack = true;
        }
        else
        {
            attack = false;
        }
        
        animator.SetBool("Attack", attack);
        animator.SetBool("Broken", !tActive);
        animator.SetInteger("Level", level);
        animator.SetInteger("Weapon", currentWeapon);
        
        if (health > maxHealth)
        {
            health = maxHealth;
        } 
        else if (health < 0)
        {
            health = 0;
        }
        
        if (health > 0)
        {
            tActive = true;
        }
        else
        {
            tActive = false;
        }

        if (boxTimer > 0)
        {
            boxTimer -= Time.deltaTime;
        }
        else
        {
            placing1.SetActive(false);
            placing2.SetActive(false);
            placing3.SetActive(false);
            repair.SetActive(false);
            upgrade.SetActive(false);
            playerInRange = false;
        }

        if (playerInRange)
        {
            boxPlacing();
        
        if (level == 0)
        {
            if (input1 && player.GetComponent<PlayerController>().money >= weapon[0].stat[level].sentryPrices)
            {
                input1 = false;
                Debug.Log("Repair1");
                
                currentWeapon = 0;
                player.GetComponent<PlayerController>().money -= weapon[currentWeapon].stat[level].sentryPrices;
                arrayRead();
                level++;
                StartCoroutine("Repair");
            }
            if (input2 && player.GetComponent<PlayerController>().money >= weapon[1].stat[level].sentryPrices)
            {
                input2 = false;
                Debug.Log("Repair2");
                
                currentWeapon = 1;
                player.GetComponent<PlayerController>().money -= weapon[currentWeapon].stat[level].sentryPrices;
                arrayRead();
                level++;
                StartCoroutine("Repair");
            }
            if (input3 && player.GetComponent<PlayerController>().money >= weapon[2].stat[level].sentryPrices)
            {
                input3 = false;
                Debug.Log("Repair3");
                
                currentWeapon = 2;
                player.GetComponent<PlayerController>().money -= weapon[currentWeapon].stat[level].sentryPrices;
                arrayRead();
                level++;
                StartCoroutine("Repair");
                
            }
        }
        else
        {
            if (health > 0)
            {
                if (inputReturn)
                {
                    inputReturn = false;
                    Debug.Log("Ok1");
                
                    int playerMoney = player.GetComponent<PlayerController>().money;
                
                    Debug.Log(playerMoney);
                    Debug.Log(weapon[currentWeapon].stat[level].sentryPrices);
                
                    if ( playerMoney >= weapon[currentWeapon].stat[level].sentryPrices)
                    {
                        Debug.Log("Ok2");
                        player.GetComponent<PlayerController>().money -= weapon[currentWeapon].stat[level].sentryPrices;
                        arrayRead();
                        level++;
                    }
                }
            }

            {
                if (inputSpace)
                {
                    Debug.Log("Click");
                    
                    inputSpace = false;
                    int playerMoney = player.GetComponent<PlayerController>().money;
            
                    if ( playerMoney >= weapon[currentWeapon].stat[level-1].repairPrices)
                    {
                        player.GetComponent<PlayerController>().money -= weapon[currentWeapon].stat[level-1].repairPrices;
                        StartCoroutine("Repair");
                    }
                }
            }
        }
        }
    }

    public void Attack(GameObject target)
    {
        if (tActive)
        {
            Vector3 dir = target.transform.position - transform.position;
            float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg - 90f;
            transform.localRotation = Quaternion.AngleAxis(angle, Vector3.forward);
            
            animTimer = 0.2f;
            if (timer <= 0)
            {
                timer = shootRate;
                
                GameObject bullet = Instantiate(prefabBullet, transform.position, Quaternion.AngleAxis(angle + 90, Vector3.forward));
                if (bullet.GetComponent<BulletScript>() != null)
                {
                    bullet.GetComponent<BulletScript>().speed = bulletSpeed;
                    bullet.GetComponent<BulletScript>().damage = damage; 
                }
                else
                {
                    bullet.GetComponent<GrenadeScript>().speed = bulletSpeed;
                    bullet.GetComponent<GrenadeScript>().damage = damage;
                }
            }
            //Attack ennemi
        }
    }

    public void detectRepair(Collider2D other)
    {
        boxTimer = 0.1f;
        playerInRange = true;
    }

    public void arrayRead()
    {
        shootRate = weapon[currentWeapon].stat[level].shootRate;
        damage = weapon[currentWeapon].stat[level].damage;
        maxHealth = weapon[currentWeapon].stat[level].newMaxHealth;
        regenPerSec = weapon[currentWeapon].stat[level].regenPerSec;
        prefabBullet = weapon[currentWeapon].prefabBullet;
    }

    public void boxPlacing()
    {
        boxTimer = 0.1f;
        if (level > 0)
        {
            upgrade.SetActive(true);
            repair.SetActive(true);
            upgrade.GetComponent<Animator>().SetInteger("Money", player.GetComponent<PlayerController>().money);
            repair.GetComponent<Animator>().SetInteger("Money", player.GetComponent<PlayerController>().money);
            upgrade.GetComponent<Animator>().SetInteger("Level", level);
            repair.GetComponent<Animator>().SetInteger("Level", level);
            placing1.SetActive(false);
            placing2.SetActive(false);
            placing3.SetActive(false);
        }
        else
        {
            upgrade.SetActive(false);
            repair.SetActive(false);
            placing1.SetActive(true);
            placing2.SetActive(true);
            placing3.SetActive(true);
        }
    }
    
    public IEnumerator Repair()
    {
        player.GetComponent<PlayerMovement>().playerCanMove = false;

        while (health < maxHealth)
        {
            health += regenPerSec * Time.deltaTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        player.GetComponent<PlayerMovement>().playerCanMove = true;
        
        yield return null;
    }

    public void takeDamage(float damageTaken)
    {
        health -= damageTaken;
        StartCoroutine(blinck());
    }

    public IEnumerator blinck()
    {
        blinckTimer = 2;
        while (blinckTimer > 0)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            yield return new WaitForSeconds(0.2f);
            GetComponent<SpriteRenderer>().color = Color.white;
            yield return new WaitForSeconds(0.2f);
        }
    }
}
