﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float speed;
    public int damage;
    
    void Update()
    {
        transform.Translate(speed * Vector3.right * Time.deltaTime);
        Destroy(gameObject, 3);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<MonsterHealth>().takeDamage(damage);
            Destroy(gameObject);
        }
    }
}
