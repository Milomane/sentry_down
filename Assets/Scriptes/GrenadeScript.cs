﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeScript : MonoBehaviour
{
    public float speed;
    public int damage;
    public GameObject prefabExplosion;
    
    void Update()
    {
        transform.Translate(speed * Vector3.right * Time.deltaTime);
        Destroy(gameObject, 3);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Hey");
        if (other.tag == "Enemy")
        {
            Debug.Log("Hey");
            GameObject o = Instantiate(prefabExplosion, transform.position, Quaternion.identity);
            o.GetComponent<HurtAnimGrenade>().damage = damage;
            Destroy(gameObject);
        }
    }
}
