﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healthBar : MonoBehaviour
{
    public SentryScript sentryScript;
    void Update()
    {
        float scale = sentryScript.health / sentryScript.maxHealth;
        transform.localScale = new Vector3(scale, 1, 1);
    }
}