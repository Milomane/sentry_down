﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectRange : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Sentry")
        {
            if (GetComponentInParent<EnemyAttack>() == null)
            {
                GetComponentInParent<PoomerAttack>().Attack(other.gameObject);
            }
            else
            {
                GetComponentInParent<EnemyAttack>().Attack(other.gameObject);
            }
        }
    }
}
