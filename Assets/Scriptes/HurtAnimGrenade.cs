﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtAnimGrenade : MonoBehaviour
{
    public int damage;
    void Start()
    {
        Debug.Log("Explosion");
        Destroy (gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        Debug.Log("Explosion2");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<MonsterHealth>().takeDamage(damage);
        }
    }
}
