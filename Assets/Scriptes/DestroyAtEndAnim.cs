﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAtEndAnim : MonoBehaviour
{
    public int damage;
    void Start()
    {
        Destroy (gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Sentry")
        {
            other.GetComponent<SentryScript>().takeDamage(damage);
        }
    }
}
