﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectEnemy : MonoBehaviour
{
    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            GetComponentInParent<SentryScript>().Attack(other.gameObject);
        }
    }
}
