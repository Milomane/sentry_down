﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Random = UnityEngine.Random;

public class blood : MonoBehaviour
{
    public float minFadeTime;
    public float maxFadeTime;
    
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;
    public Sprite sprite4;


    public void Start()
    {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        int i = Mathf.RoundToInt(Random.Range(0, 5));
        switch (i)
        {
            case 0:
                renderer.sprite = sprite1;
                break;
            case 1:
                renderer.sprite = sprite2;
                break;
            case 2:
                renderer.sprite = sprite3;
                break;
            case 3:
                renderer.sprite = sprite4;
                break;
        }
        
        float fadeTime = Mathf.RoundToInt(Random.Range(minFadeTime, maxFadeTime));
        
        GetComponent<SpriteRenderer>().DOFade(0, fadeTime);
        Destroy(gameObject, fadeTime);
    }
}
