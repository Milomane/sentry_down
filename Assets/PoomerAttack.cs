﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoomerAttack : MonoBehaviour
{
    public int damage;
    public float attackRate;
    
    public float timer;
    private Animator animator;

    public int baseDamage;
    public GameObject prefabBullet;
    
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            timer = 0;
        }
    }

    public void Attack(GameObject target)
    {
        Vector3 dir = target.transform.position - transform.position;
        float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg - 90f;
        
        if (timer <= 0)
        {
            if (target.GetComponent<SentryScript>().health > 0)
            {
                GameObject bullet = Instantiate(prefabBullet, transform.position, Quaternion.AngleAxis(angle + 90, Vector3.forward));
                bullet.GetComponent<PoomerBulletScript>().damage = damage;
                
                timer = attackRate;
                animator.SetTrigger("Attack");
            }
        }
    }
}