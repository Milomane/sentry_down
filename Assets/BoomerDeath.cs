﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerDeath : MonoBehaviour
{
    public int damage;
    public GameObject explosionPrefab;
    
    public void Explode()
    {
        GameObject o = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        o.GetComponent<DestroyAtEndAnim>().damage = damage;
    }
}
